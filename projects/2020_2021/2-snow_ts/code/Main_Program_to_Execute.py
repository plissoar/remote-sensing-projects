# -*- coding: utf-8 -*-
"""
Created on Wed May 27 21:50:59 2020

@author: pierr
"""

"""This program was made during a school project about satellite remote sensing
in the ENSE3 engineering school, Grenoble, France. The whole report about this 
project and the tools we used is avalaible on :
https://gricad-gitlab.univ-grenoble-alpes.fr/demarspi/t-l-detection-modis/-/tree/master"""


# =============================================================================
# External Packages ---
# Be careful to verifiy they are all installed in your Anaconda 
# environnement which isn't your base(root) environnement.
# =============================================================================
import os
import sys
from getpass import getpass
import Download_HDF as D
import compute_and_save as cs
import glob
# =============================================================================
# Internal Modules ---
# Those modules come from the folder "script" in .zip file you downloaded on 
# https://gricad-gitlab.univ-grenoble-alpes.fr/demarspi/t-l-detection-modis/-/tree/master
# =============================================================================
#import preview_and_save as ps
# =============================================================================
# Functions
# =============================================================================

def delete_tif():
    """Delete all .tif files in the MAIN_FOLDER"""
    path = os.path.join(os.path.dirname(os.getcwd()),'*.tif')
    files = glob.glob(path)
    for file in files:
        os.remove(file)
        
def EarthData_Connexion():
    """
    Connect to your Earthdata account, if you don't have one go create one on Earthdata

    Returns
    -------
    username : char
        username used by the download functions
    password : TYPE
        password used by the download functions

    """
    print("Connexion Identifiants for EarthData website :")
    username = ''

    # For Python 2/3 compatibility:
    try:
        do_input = raw_input  # noqa
    except NameError:
        do_input = input

    while not username:
        username = do_input('Earthdata username: ')
    password = ''
    while not password:
        password = getpass('Password: ')
    
    return username, password
    
def get_date():
    """Ask the user the date to consider for the main programm
    
    Returns
    -------
    month,day with 0x format.
    """
    ok = False
    while not ok:
        print("________________________________________________________________\n")
        print("Enter a date in 2019 ----------------------------")
        month = input("The month number : ")
        day = input("The day number in the this month : ")
        int_month = int(month)
        int_day = int(day)
        if len(month) == 1:
            month = '0{}'.format(month)
        if len(day) == 1:
            day = '0{}'.format(day)
        
        if month == '02' and int_day > 28:
            print("Error wrong date")
            return
        elif (month == '04' or month == '06' or month == '09' or month == '11') and int_day == 31:
            print("Error wrong date")
            return
        elif int_month <= 12 and int_month > 0 and int_day > 0 and int_day <= 31:
            ok = True
        else: 
            print("Error wrong date")
            return
        
        return month,day

def get_day(month, day):    
    """Function that convert the date into the day in the year"""
    int_month = int(month)
    int_day = int(day)
    num = 0
    ls = [31,28,31,30,31,30,31,31,30,31,30,31]
    if month == '01':
        num = int_day
    else:
        for i in range(int_month-1):
            num += ls[i]
        num += int_day
    if num < 10:
        days = "00{}".format(num)
    elif num < 100:
        days = "0{}".format(num)
    else:
        days = str(num)
    return days


def select_file(day, month, GEO_ZONE):    
    """Get infos on the type of data needed and if we need to download it"""
    """The programm offers 5 possibilities, 3 different views of the alps earth cells"""
    """and offers 2 possibilities of closest view shaped around the Isère french departement"""
    chosen = False
    while not chosen:
        print("________________________________________________________________\n")
        print("Current date to consider : ", day,"/",month,"\n")       
        print("Current geographical zone to consider : ", GEO_ZONE, "\n")
        print("----------------------------------------------------------------")
        print("Choose a type of file about this zone to download (if necessary), preview and save (if wanted) : \n")
        print ("1 - RGB View of the",GEO_ZONE," \n") #compute with MOD09GA
        print ("2 - NDSI Snow Cover 5km resolution of the",GEO_ZONE,"\n") #compute with MOD10C1
        print ("3 - NDSI Snow Cover 500m resolution of the",GEO_ZONE,"\n") #compute with MOD10A1
        print ("4 - RGB View of the french departement Isère\n") #compute with MOD09GA + shape of Isère (French departement)
        print ("5 - NDSI Snow Cover 500m resolution of the french departement Isère\n") #compute with MOD10A1+ shape of Isère (French departement)
        print("----------------------------------------------------------------")
        
        exist = False
        
        data = input("Enter the number of the type of file you want : ")
        days = get_day(month,day)
        
        if data == '1' :
            print("---> You choosed the type : 1 - RGB View of the",GEO_ZONE,"")
            FILE_TYPE = "RGB"
            FILE_NAME = "MOD09GA.A2019{0}.h18v04.006.hdf".format(days)
            chosen = True
        
        if data == '2' :
            print("---> You choosed the type : 2 - NDSI Snow Cover 5km resolution of the zone")
            FILE_TYPE = "NDSI_5km"
            FILE_NAME = "MOD10C1.A2019{0}.006.hdf".format(days)
            chosen = True
            
        if data == '3' :
            print("---> You choosed the type : 3 - NDSI Snow Cover 500m resolution of the zone")
            FILE_TYPE = "NDSI_500m"
            FILE_NAME = "MOD10A1.A2019{0}.h18v04.006.hdf".format(days)
            chosen = True
            
        if data == '4' :
            print("---> You choosed the type : 4 - RGB View of the french departement Isère")
            FILE_TYPE = "RGB_cropped"
            FILE_NAME = "MOD09GA.A2019{0}.h18v04.006.hdf".format(days)
            chosen = True
            
        if data == '5' :
            print("---> You choosed the type : 5 - NDSI Snow Cover 500m resolution of the french departement Isère")
            FILE_TYPE = "NDSI_500m cropped"
            FILE_NAME = "MOD10A1.A2019{0}.h18v04.006.hdf".format(days)
            chosen = True
        
        #Verifying if the product exists already 
        existing_files = os.listdir(os.path.dirname(os.getcwd()))
        for i in range(len(existing_files)):
            if not os.path.isdir(existing_files[i]):
                if ((os.path.splitext(FILE_NAME)[0]) in existing_files[i]) and (not (".png" in existing_files[i])) :
                    exist = True
                    FILE_NAME = existing_files[i]
      
    return data, FILE_TYPE, FILE_NAME, exist


def download_hdf_file(data, month,day, GEO_ZONE):
    """Starts the download associated with the chosen product"""
    if data == '1' :
        print("________________________________________________________________\n")
        print("---> Downloading the type : 1 - RGB View of the",GEO_ZONE," for", day,"/",month,"/ 2019")
        FILE_NAME = D.download_MOD09GA( month, day, username, password)
        
    if data == '2' :
        print("---> Downloading the type : 2 - NDSI Snow Cover 5km resolution of the",GEO_ZONE,"for", day,"/",month,"/ 2019")
        FILE_NAME = D.download_MOD10C1( month, day, username, password)

    if data == '3' :
        print("---> Downloading the type : 3 - NDSI Snow Cover 500m resolution of the",GEO_ZONE,"for", day,"/",month,"/ 2019")
        FILE_NAME = D.download_MOD10A1( month, day, username, password)
         
    if data == '4' :
        print("---> Downloading the type : 4 - RGB View of the french departement Isère for", day,"/",month,"/ 2019")
        FILE_NAME = D.download_MOD09GA( month, day, username, password)
            
    if data == '5' :
        print("---> Downloading the type : 5 - NDSI Snow Cover 500m resolution of the french departement Isère for", day,"/",month,"/2019")
        FILE_NAME = D.download_MOD10A1(month, day, username, password)
    
    
    return FILE_NAME


def downloaded(FILE_NAME):
    
    """Verify if the download of the file_name file is over"""
    exist = False
    existing_files = os.listdir(os.path.dirname(os.getcwd()))
    for i in range(len(existing_files)) :
        if not os.path.isdir(existing_files[i]):
                if (os.path.splitext(FILE_NAME)[0]) in existing_files[i] :
                    exist = True
    return exist
        

def compute_and_save(product, file_name, month, day, GEO_ZONE):
    """Plot the product  """
    if product == '1' :
        print("________________________________________________________________\n")
        print("---> Compute and save the type : 1 - RGB View of the",GEO_ZONE,"with the file", file_name,"for", day,"/",month,"/ 2019")
        cs.compute_and_save_MOD09GA(file_name, month, day)
        
    if product == '2' :
        print("---> Compute and save the type : 2 - NDSI Snow Cover 5km resolution of the zone",GEO_ZONE,"with the file", file_name,"for", day,"/",month,"/ 2019")
        cs.compute_and_save_MOD10C1(file_name, month, day)

    if product == '3' :
        print("---> Compute and save the type : 3 - NDSI Snow Cover 500m resolution of the zone",GEO_ZONE,"with the file", file_name,"for", day,"/",month,"/ 2019")
        cs.compute_and_save_MOD10A1(file_name, month, day)
         
    if product == '4' :
        print("---> Compute and save the type : 4 - RGB View of the french departement Isère with the file", file_name,"for", day,"/",month,"/ 2019")
        cs.compute_and_save_MOD09GA_cropped(file_name, month, day)
            
    if product == '5' :
        print("---> Compute and save the type : 5 - NDSI Snow Cover 500m resolution of the french departement Isère with the file", file_name,"for", day,"/",month,"/ 2019")
        cs.compute_and_save_MOD10A1_cropped(file_name, month, day)
  
# =============================================================================
# Main
# =============================================================================
def main():
    """
    Main program to execute the automatized retrieving of Alps around Grenoble area
    MODIS HDF files of 2019 [MOD09GA,MOD10C1,MOD10A1](if necessary) and their plotting
    (if wanted) and saving in .png. 
    With this program, you enter a date in 2019, then you can activate the download
    of the MOD09GA,MOD10C1,MOD10A1 HDF files of this date (if necessary), then the preview of those
    products and the saving on .png of those plots if wanted.

    Returns
    -------
    None.
    """
    GEO_ZONE = 'Alps'
    print("________________________________________________________________\n")
    print("""Welcome in the program of an automatized retrieving of Alps around Grenoble area MODIS HDF files of 2019(if necessary) and their computing and saving in .png.\n""")
    print("""This program was made during a school project about satellite remote sensing in the ENSE3 engineering school, Grenoble, France. The whole report about this project and the tools we used is avalaible on the gitlab of the project.\n""")
    print("________________________________________________________________\n")
    print("________________________________________________________________\n")
    global username, password
    username, password = EarthData_Connexion()
    
    month,day = get_date()
    selected_product, file_type, file_name, exist = select_file(day, month, GEO_ZONE)  #Then the type of file and if we need to download it
    #if the selected_product doesn't in the main folder, download it
    if not exist : 
        file_name = download_hdf_file(selected_product, month, day, GEO_ZONE)
    else : 
        print("\nYou already have this file in your folder, the program won't download it again.\n")
    #Verify the download is over + loop to stop the execution if not found
    if not downloaded(file_name):
        print("\nThe download has failed. \n")
        sys.exit()
    #Compute and save the selected product
    compute_and_save(selected_product,file_name,month,day,GEO_ZONE)
    print("________________________________________________________________\n")
    print("The files have been succesfully processed ! \n")
    print("They are available in the tel-detect folder, where you empty the .zip from gitlab.\n")
    print("Thank you for using our program ! \n")
    return
  
if __name__ == '__main__':
    main()
    delete_tif() #Delete the intermediary .tif created, can be removed to keep them